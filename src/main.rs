// apps contains modules for service callbacks
mod apps;
// server creates the actix-web server
mod server;
use server::start_server;


fn main () {
	let _server_ok = start_server();
}