use actix_web::web;
use actix_web::{
	guard,
	App,
	HttpServer,
	HttpResponse
};
use crate::apps::{
	home,
	info,
	name,
	utils
};


#[actix_rt::main]
pub async fn start_server() -> std::io::Result<()> {
  HttpServer::new(|| {
     App::new()
      .route("/", web::get().to(home::index))
      .route("/info", web::get().to(info::index))
      .route("/name/{name}", web::get().to(name::index))
      .default_service(
     		web::resource("")
      		.route(web::get().to(utils::p404))
      		.route(web::route()
      			.guard(guard::Not(guard::Get()))
      			.to(HttpResponse::MethodNotAllowed)
      		)
      )
  })
  .bind("127.0.0.1:8088")?
  .run()
  .await
}
