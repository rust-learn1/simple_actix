use actix_web::{web, Responder, HttpResponse, HttpRequest};


pub async fn index(req: HttpRequest, info: web::Path<(String,)>) -> impl Responder {
	println!("{:?}", req);
	let response = format!("Hola amiga {}", info.0);
  return HttpResponse::Ok().body(response);
}