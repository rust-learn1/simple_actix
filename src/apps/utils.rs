use actix_files as fs;
use actix_web::{
  Result,
};
use actix_web::http::{StatusCode};

pub async fn p404() -> Result<fs::NamedFile> {
  Ok(fs::NamedFile::open("static/404.html")?.set_status_code(StatusCode::NOT_FOUND))
}